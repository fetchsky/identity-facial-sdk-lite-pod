Pod::Spec.new do |s|
    s.name           = "FacialRecognition"
    s.version        = "0.0.150"
    s.summary        = "Face Recognition with camera"
    s.description    = "Score for simalirity of faces given."
    s.license        = "Apache 2.0"
    s.author         = "Fetch Sky"
    s.homepage       = "https://gitlab.com/fetchsky/kashf/identity/frontend/facial-recognition-pod"
    s.platform       = :ios, "10.0"
    s.source         = { :git => "https://gitlab.com/fetchsky/kashf/identity/frontend/facial-recognition-pod.git", :branch => "main" }
    # s.source_files   = "*.{h,m}"
    s.requires_arc   = true
    s.vendored_frameworks = "FacialRecognition.xcframework"
  end
  